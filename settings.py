import pygame
import title
from pygame.locals import *

def settings():

    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode((1435, 765))
    pygame.display.set_caption("Texas Hold 'em ")

    # Fill background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 100, 0))

    # Display some text
    font = pygame.font.Font(None, 36)
    text = font.render("Settings", 1, (10, 10, 10))
    textpos = text.get_rect()
    textpos.centerx = background.get_rect().centerx
    background.blit(text, textpos)


    # stores the width of the
    # screen into a variable
    width = screen.get_width()
  
    # stores the height of the
    # screen into a variable
    height = screen.get_height()

    button_color = (255,255,255)
    smallfont = pygame.font.SysFont(None, 35)
    back_text = smallfont.render('Quit' , True , button_color)

    button_height = 40
    button_width = 140
    
    back_button_X = width/2-70
    back_button_Y = height-100
    # light shade of the button
    color_light = (170,170,170)
    
    # dark shade of the button
    color_dark = (100,100,100)

    # Blit everything to the screen
    screen.blit(background, (0, 0))

    # Event loop
    while 1:
        for ev in pygame.event.get():
          
            if ev.type == pygame.QUIT:
                pygame.quit()
                
            #checks if a mouse is clicked
            if ev.type == pygame.MOUSEBUTTONDOWN:
                
                #if the mouse is clicked on the
                # button the game is terminated
                if back_button_X <= mouse[0] <= back_button_X+140 and back_button_Y <= mouse[1] <= back_button_Y+40:
                    title.title()

        #get mous position
        mouse = pygame.mouse.get_pos()

        #disply quit box
        if back_button_X <= mouse[0] <= back_button_X+140 and back_button_Y <= mouse[1] <= back_button_Y+40:
            pygame.draw.rect(screen,color_light,[back_button_X,back_button_Y,140,40])
        else:
            pygame.draw.rect(screen,color_dark,[back_button_X,back_button_Y,140,40])
      
        # superimposing the text onto our button
        screen.blit(back_text , (back_button_X, back_button_Y))

        # updates the frames of the game
        pygame.display.update()

        screen.blit(background, (0, 0))
