import pygame
from Game import *
from playButtons import display_check_bet, display_fold_call_raise
import settings
from pygame.locals import *
import Card
import time
import playButtons


width = 1435
height = 765
center = [width / 2, height / 2]
seat_cors = [[660, 680],[405,590],[290, 360],[405,135],[660, 55],
             [920,135],[1045, 360],[920,590]] #the positions of the 8 seats at the table
background = None
screen = pygame.display.set_mode((width, height))

game = None



compare_hands = False #whether it is time for all players to show hands and show the winner


#drawss a given card at a given coordinate, face up or down, folded or unfolded
def display_card(card, cors, face_up, folded):

    if(card != None):
        #face up card
        path = Card.get_card_image_path(card)
        image = pygame.image.load(path)
        image = pygame.transform.scale(image, (65, 90))
    else:
        #face down card
        image = pygame.image.load("card-back.png")
        image = pygame.transform.scale(image, (65, 90))

    screen.blit(image, cors)

    if(folded):
        folded = pygame.Rect(cors[0], cors[1], 65, 90)
        pygame.draw.rect(screen, (100,100,100,5), folded)




#displays the user's hand face up, and the computers' hands face down
def display_hands():

    global compare_hands
    global seat_cors
    if(compare_hands):
        for i, player in enumerate(game.players):
            if(player.is_in):
                display_card(player.hand[0],(seat_cors[i][0] + 70, seat_cors[i][1]), True, False)
                display_card(player.hand[1],(seat_cors[i][0] + 90, seat_cors[i][1] + 5), True, False)
    else:
        for i, player in enumerate(game.players):
            cards_folded = not player.is_in
            if (type(player).__name__ == "UserPlayer"):
                display_card(player.hand[0],(seat_cors[i][0] + 70, seat_cors[i][1]), True, cards_folded)
                display_card(player.hand[1],(seat_cors[i][0] + 90, seat_cors[i][1] + 5), True, cards_folded)
            else:
                display_card(None, (seat_cors[i][0] + 70, seat_cors[i][1]), False, cards_folded)
                display_card(None, (seat_cors[i][0] + 90, seat_cors[i][1] + 5), False, cards_folded)

def display_bets(game, screen):
    """Assumes that the game.bets and game.players are ordered together."""
    for i, bet in enumerate(game.bets):
        player_loc = seat_cors[i]
        x, y = player_loc[0] + 10, player_loc[1]
        font = pygame.font.SysFont('Arial', 20)
        pygame.draw.rect(screen, (41, 103, 0), (x, y - 20, 100, 20))
        screen.blit(font.render(f'Bet: ${game.bets[i]:,}', True, (0, 0, 0)), (x, y - 20))


def end_screen(screen):
    screen.fill((41, 103, 0))
    pygame.display.update()
    font = pygame.font.SysFont('Arial', 20)
    WHITE = (255, 0, 0)
    BLUE = (0, 0, 255)
    play = pygame.draw.rect(screen, WHITE, (100, 100, 150, 50))
    screen.blit(font.render('Play Again!', True, (0,0,0)), (100, 100))
    quit_ = pygame.draw.rect(screen, BLUE, (500, 100, 150, 50))
    screen.blit(font.render('I Wanna Quit!', True, (0,0,0)), (500, 100))

    events = pygame.event.get()
    for event in events:

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = event.pos  # gets mouse position

            # checks if mouse position is over the button

            if play.collidepoint(mouse_pos):
                return True
            elif quit_.collidepoint(mouse_pos):
                return False


#calls functions of the Game object, and calls update_display() to show those changes to the game
def play():
    while(True):
        global game
        global compare_hands
        game = Game(20, 1000, 5)  #TODO: incorporate settings into intializing game
        # dealer_chip()
        game.deal_initial()
        display(1)


        game.start_pre_flop_betting()
        while(game.betting_ongoing):
            player = enumerate(game.players)
            if (game.current_bet == 0 and type(player).__name__ == "UserPlayer"):
                choice = playButtons.display_check_bet(screen)
            else:
                choice = playButtons.display_fold_call_raise(screen)
            game.get_next_bet(choice)
            display(1)

        game.show_flop()
        display(1)

        game.start_betting()
        while (game.betting_ongoing):
            game.get_next_bet()
            display(1)

        game.show_turn()
        display(1)

        game.start_betting()
        while (game.betting_ongoing):
            game.get_next_bet()
            display(1)
        game.show_river()

        game.start_betting()
        while (game.betting_ongoing):
            game.get_next_bet()
            display(1)

        compare_hands = True
        game.determine_winner()
        display(5)
        compare_hands = False

        game.distribute_pot()
        game.next_round()




#draws the screen, taske in a float that is the number of seconds the screen will be displayed for
def display(display_time: float):
    start_time = time.time()
    current_time = 0

    while(current_time < display_time):
        for ev in pygame.event.get():
            if ev.type == pygame.QUIT:
                pygame.quit()

        player = enumerate(game.players)
        if (game.current_bet == 0 and type(player).__name__ == "UserPlayer"):
            choice = playButtons.display_check_bet(screen)
        else:
            choice = playButtons.display_fold_call_raise(screen)

        display_players()
        table = pygame.Rect(400, 150, 600, 500)
        pygame.draw.ellipse(screen, (105, 52, 0), table)

        # colors
        black = (0,0,0)

        display_community_cards()

        display_bets(game, screen)

        display_pot()

        display_hands()

        display_dealer_chip()

        display_turn()

        if (compare_hands):
            display_winner()

        # updates the frames of the game
        pygame.display.update()
        current_time = time.time() - start_time






#displays the community cards
def display_community_cards():
    x_cor = 520
    y_cor = 350
    cors = [(x_cor + 70 * 0,y_cor),(x_cor + 70 * 1,y_cor),(x_cor + 70 * 2,y_cor),
            (x_cor + 70 * 3,y_cor),(x_cor + 70 * 4,y_cor)]
    for i, card in enumerate(game.community_cards):
        display_card(card, cors[i], True, False)

#draws the seats, player names, chip count, and hand
def display_players():
    font = pygame.font.SysFont('Arial', 12)
    for i, seat_cor in enumerate(seat_cors):
        seat = pygame.Rect(seat_cor[0], seat_cor[1], 70, 70)
        pygame.draw.rect(screen, (175, 0, 42), seat)
        if (i < len(game.players)):
            screen.blit(font.render(game.players[i].name, True, (0, 0, 0)), (seat_cor[0] + 10, seat_cor[1] + 40))
            screen.blit(font.render(str(game.players[i].chips), True, (0, 0, 0)), (seat_cor[0] + 10, seat_cor[1] + 55))

            display_hands()



# Alex's Implementations 

#1. implement pot and updating/distribution pot
def show_blinds():
   
    font = pygame.font.SysFont('Arial', 20)
    screen.blit(font.render("Big Blind: " + (str(game.big_blind)), True, (0, 0, 179)), (1200, 100))
    screen.blit(font.render("Small Blind: " + (str(game.small_blind)), True, (0, 0, 179)), (1200, 130))




# At the beginning of the round, initialize pot object with corresponding graphic (i.e pot of gold)
def display_pot():
    font = pygame.font.SysFont('Arial', 20)
    screen.blit(font.render("Pot:" + (str(game.pot)), True, (0, 0, 0)), (660, 300))


# call determine_winner(), and distribute pot integer to whichever player has the best hand.  The integer
#representing the pot of gold is set to 0 at this point.  
def display_winner():
    font = pygame.font.SysFont('Arial', 30)
    if(len(game.winners) == 1):
        screen.blit(font.render(game.players[game.winners[0]].name + " wins the round.", True, (0, 0, 0)), (500,500))
    else:
        screen.blit(font.render(game.players[game.winners[0]].name + "Multiple players win the round:", True, (0, 0, 0)), (500, 500))
        for i, winner in enumerate(game.winners):
            screen.blit(font.render(game.players[game.winners[0]].name, True, (0, 0, 0)), (700, 200 + 100 * i))




def display_dealer_chip():
    pygame.font.init()
    font = pygame.font.SysFont('Arial', 14)
    dealer = game.dealer
    pygame.draw.circle(screen, (128, 212, 255),(seat_cors[dealer][0] - 50, seat_cors[dealer][1] + 30), 30, 30)
    text_surface = font.render("Dealer", True, (0, 0, 0))
    screen.blit(text_surface, (seat_cors[dealer][0] - 65,seat_cors[dealer][1] + 20))

def display_turn():
    pygame.font.init()
    font = pygame.font.SysFont('Arial', 14)
    turn = game.current_turn
    pygame.draw.rect(screen, (230,230,230), (seat_cors[turn][0] + 24, seat_cors[turn][1] + 10, 20 ,20), 30, 30)
