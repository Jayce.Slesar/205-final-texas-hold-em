"""Chips class for handling chip interactions"""
from enum import Enum


class ChipValue(Enum):
    """Chip value"""
    WHITE = 5
    RED = 10
    GREEN = 50
    BLUE = 100
    BLACK = 200


"""Chip funcs for handling chip interactions"""
def bin_chips_by_value(amount: int):
    """Will optimally bin a value into various chip types from the ChipValue class."""
    num_black_chips = amount // ChipValue.BLACK.value
    amount -= num_black_chips * ChipValue.BLACK.value

    num_blue_chips = amount // ChipValue.BLUE.value
    amount -= num_blue_chips * ChipValue.BLUE.value

    num_green_chips = amount // ChipValue.GREEN.value
    amount -= num_green_chips * ChipValue.GREEN.value

    num_red_chips = amount // ChipValue.RED.value
    amount -= num_red_chips * ChipValue.RED.value

    num_white_chips = amount // ChipValue.WHITE.value
    amount -= num_white_chips * ChipValue.WHITE.value

    return {'black': num_black_chips, 'blue': num_blue_chips, 'green': num_green_chips, 'red': num_red_chips, 'white': num_white_chips}


def get_value_from_chips(chips: dict):
    """Get the value in dollars from a dictionary representing the amount of chips a player has."""
    value = 0

    value += chips['black'] * ChipValue.BLACK.value
    value += chips['blue'] * ChipValue.BLUE.value
    value += chips['green'] * ChipValue.GREEN.value
    value += chips['red'] * ChipValue.RED.value
    value += chips['white'] * ChipValue.WHITE.value

    return value
