import copy
def get_hand_rankings(hands):
    hand_rankings = []

    for hand in hands:
        # check if hand was folded
        if (hand == None):
            hand_rankings.append([0])
            continue

        hand = sorted(hand, key=lambda x: x.value, reverse=True)

        straight = is_straight(hand)
        flush = is_flush(hand)
        four_of_a_kind = is_four_of_a_kind(hand)
        full_house = is_full_house(hand)
        three_of_a_kind = is_three_of_a_kind(hand)
        two_pair = is_two_pair(hand)
        pair = is_pair(hand)
        high_card = get_high_card(hand)



        # check for royal flush and straight flush
        if (flush and is_straight(hand) and len(flush) == 5):
            hand_rankings.append([9, flush[0].value])

        # check for four of a kind
        elif (len(four_of_a_kind) == 5):
            hand_rankings.append([8, four_of_a_kind[0].value, four_of_a_kind[4].value])

        # check for full house
        elif (len(full_house) == 5):
            hand_rankings.append([7, full_house[0].value, full_house[4].value])

        # check for flush
        elif (len(flush) == 5):
            hand_rankings.append([6, flush[0].value])

        # check for straight
        elif (len(straight) == 5):
            hand_rankings.append([5, straight[0].value])

        # check for three of a kind
        elif (len(three_of_a_kind) == 5):
            hand_rankings.append([4, three_of_a_kind[0].value, three_of_a_kind[3].value, three_of_a_kind[4].value])

        # check for two pair
        elif (len(two_pair) == 5):
            hand_rankings.append([3, two_pair[0].value, two_pair[2].value, two_pair[4].value])

        # check for pair
        elif (len(pair) == 5):
            hand_rankings.append([2, pair[0].value, pair[2].value, pair[3].value, pair[4].value])

        # check for high card
        else:
            hand_rankings.append(
                [1, high_card[0].value, high_card[1].value, high_card[2].value, high_card[3].value, high_card[4].value])

        if(len(hand_rankings[-1]) < 6):
            while (len(hand_rankings[-1]) < 6):
                hand_rankings[-1].append(0)


    return (hand_rankings)


# takes in 7 cards. if there is a straight, returns the best 5 straight cards. if not, returns empty list
def is_straight(hand):
    consecutive_card_count = 1

    for i in range(1, len(hand)):
        if(hand[i-1].value - hand[i].value == 0):
            continue
        if (hand[i - 1].value - hand[i].value == 1):
            consecutive_card_count += 1
            if (consecutive_card_count == 5):
                return hand[i - 4:i + 1]
        else:
            consecutive_card_count = 1

    return []


# takes in 7 cards. if there is a flush, returns the 5 flush cards. if not, returns empty list
def is_flush(hand):
    output = []

    spade_count = sum(card.suit == "s" for card in hand)
    diamond_count = sum(card.suit == "d" for card in hand)
    heart_count = sum(card.suit == "h" for card in hand)
    club_count = sum(card.suit == "c" for card in hand)

    if (spade_count >= 5):
        for card in hand:
            if len(output) == 5:
                break
            if (card.suit == "s"):
                output.append(card)
    elif (diamond_count >= 5):
        for card in hand:
            if len(output) == 5:
                break
            if (card.suit == "d"):
                output.append(card)
    elif (heart_count >= 5):
        for card in hand:
            if len(output) == 5:
                break
            if (card.suit == "h"):
                output.append(card)
    elif (club_count >= 5):
        for card in hand:
            if len(output) == 5:
                break
            if (card.suit == "c"):
                output.append(card)
    return output


def get_hand_values_dict(hand):
    hand_values = {}
    for card in hand:
        if card.value in hand_values:
            hand_values[card.value] += 1
        else:
            hand_values[card.value] = 1
    return hand_values


def is_four_of_a_kind(hand):
    output = []
    hand_values = get_hand_values_dict(hand)

    for value in hand_values:
        if (hand_values[value] == 4):
            added_kicker = False
            for card in hand:
                if (card.value == value):
                    output.append(card)
                else:
                    if (not added_kicker):
                        kicker = card
                        added_kicker = True
            output.append(kicker)

    return output


def is_full_house(hand):
    output = []
    hand_values = get_hand_values_dict(hand)
    for value in hand_values:
        if (hand_values[value] == 3):
            for card in hand:
                if (card.value == value):
                    output.append(card)
    if (len(output) == 0):
        return []
    for value in hand_values:
        if (hand_values[value] == 2):
            for card in hand:
                if (card.value == value):
                    output.append(card)
    return output


def is_three_of_a_kind(hand):
    output = []
    hand_values = get_hand_values_dict(hand)
    for value in hand_values:
        if (hand_values[value] == 3):
            for card in hand:
                if (card.value == value):
                    output.append(card)
            for card in hand:
                if (card.value != value and len(output) < 5):
                    output.append(card)
    return output


def is_two_pair(hand):
    output = []
    l_hand = copy.copy(hand)  # local hand variable
    hand_values = get_hand_values_dict(l_hand)
    for value in hand_values:
        if (hand_values[value] == 2):
            for card in reversed(l_hand):
                if (card.value == value and len(output) < 4):
                    output.append(card)
                    l_hand.remove(card)
    if (len(output) == 4):
        output.append(l_hand[0])
        return output
    else:
        return []


def is_pair(hand):
    output = []
    l_hand = copy.copy(hand)  # local hand variable
    hand_values = get_hand_values_dict(l_hand)
    for value in hand_values:
        if (hand_values[value] == 2):
            for card in reversed(l_hand):
                if (card.value == value):
                    output.append(card)
                    l_hand.remove(card)

    if (len(output) == 2):
        output.append(l_hand[0])
        output.append(l_hand[1])
        output.append(l_hand[2])
        return output
    else:
        return []


def get_high_card(hand):
    output = []
    for card in hand:
        if (len(output) < 5):
            output.append(card)
    return output
