import os

class Card:
    #2-14 s,h,d,c
    def __init__(self, suit, value):
        self.suit = suit
        self.value = value

    def __repr__(self):
        str_value = str(self.value)
        if(self.value == 11):
            str_value = "J"
        elif (self.value == 12):
            str_value = "Q"
        elif (self.value == 13):
            str_value = "K"
        elif (self.value == 14):
            str_value = "A"
        return str(str_value) + self.suit

    def __eq__(self, other):
        if (isinstance(other, Card)):
            return self.suit == other.suit and self.value == other.value


def get_card_image_path(card):
    all_cards = [card for card in os.listdir('assets')]

    if card.value == 11:
        value = "jack"
    elif card.value == 12:
        value = "queen"
    elif card.value == 13:
        value = "king"
    elif card.value == 14:
        value = "ace"
    else:
        value = card.value

    value = str(value)

    if card.suit == "s":
        suit = "spades"
    elif card.suit == "h":
        suit = "hearts"
    elif card.suit == 'c':
        suit = "clubs"
    elif card.suit == 'd':
        suit = "diamonds"

    for card in all_cards:
        if card.startswith(value) and suit in card:
            return os.path.join('assets', card)
