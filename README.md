# 205-final-texas-hold-em

Texas Hold 'Em Implementation with Python and Pygame
written by: Alex Downs, Jayce Slesar, Ethan West, and Paul Yarin


Summary: 
Texas Hold 'Em is a type of poker that involves two or more individuals. The goal of the game is to make the best five card hand using any combination of the two cards in the player's hand and the five community cards on the table. Each player is dealt 2 cards, and each player bets in a round of betting. Then, 3 community cards are shown and a round of betting happens. The 4th community card is shown with a round of betting. Finally the 5th and final community card is shown. A last round of betting happens, and the pot is won by the player with the best hand. The game continues on until a person takes all of the chips/money and wins.  

How to Play our Version:

1. Run the main.py file in alexGUI branch.  
2. When prompted with the home screen, select play.  
3. The game will commence and betting will start each player begins with 1000 chips.
4. When the white circle lands on your red square, you will enter 0, 1, or 2 into the console to fold, call, or raise, respectively.  
5. This will continue until the round is over and a winner is declared. 
6. To end the game early, press control C in the terminal window in whatever IDE you are using.  Happy betting!



