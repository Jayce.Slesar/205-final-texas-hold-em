import pygame
from pygame.locals import *

def display_fold_call_raise(screen):
    height = screen.get_height()
    button_height = 40
    button_width = 80
    button_color = (100,100,100)
    text_color = (0,0,0)
    smallfont = pygame.font.SysFont(None, 35)
    call_text = smallfont.render('Call' , True , text_color)
    raise_text = smallfont.render('Raise' , True , text_color)
    fold_text = smallfont.render('Fold' , True , text_color)
    
    call = pygame.draw.rect(screen, button_color, (40, height-60, button_width, button_height))
    raise_rect = pygame.draw.rect(screen, button_color, (140, height-60, button_width, button_height))
    fold = pygame.draw.rect(screen, button_color, (240, height-60, button_width, button_height))

    events = pygame.event.get()
    for event in events:

        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = event.pos  # gets mouse position

            # checks if mouse position is over the button
            if call.collidepoint(mouse_pos):
                return 0
            elif raise_rect.collidepoint(mouse_pos):
                return 1
            elif fold.collidepoint(mouse_pos):
                return 2
    
    screen.blit(call_text , (40, height-60))
    screen.blit(raise_text , (140, height-60))
    screen.blit(fold_text , (240, height-60))


def display_check_bet(screen):
    clock = pygame.time.Clock()
    height = screen.get_height()
    button_height = 40
    button_width = 80
    button_color = (100,100,100)
    text_color = (0,0,0)
    smallfont = pygame.font.SysFont(None, 35)
    check_text = smallfont.render('Check' , True , text_color)
    bet_text = smallfont.render('Bet' , True , text_color)
    
    check = pygame.draw.rect(screen, button_color, (40, height-60, button_width, button_height))
    bet = pygame.draw.rect(screen, button_color, (140, height-60, button_width, button_height))

    base_font = pygame.font.Font(None, 32)
    user_text = ''
    input_rect = pygame.Rect(240, height-60, 140, 32)
    color_active = pygame.Color('lightskyblue3')
    color_passive = pygame.Color('chartreuse4')
    color = color_passive
    
    active = False

    events = pygame.event.get()
    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSPACE:
                user_text = user_text[:-1]
            else:
                user_text += event.unicode
                
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouse_pos = event.pos  # gets mouse position

            # checks if mouse position is over the button

            if check.collidepoint(mouse_pos):
                return 5
            elif bet.collidepoint(mouse_pos):
                return user_text
            
            if input_rect.collidepoint(event.pos):
                active = True
            else:
                active = False
  
        

    screen.blit(check_text , (40, height-60))
    screen.blit(bet_text , (140, height-60))

    if active:
        color = color_active
    else:
        color = color_passive
    pygame.draw.rect(screen, color, input_rect)
    text_surface = base_font.render(user_text, True, (255, 255, 255))
    screen.blit(text_surface, (input_rect.x+5, input_rect.y+5))
    input_rect.w = max(100, text_surface.get_width()+10)
    clock.tick(60)