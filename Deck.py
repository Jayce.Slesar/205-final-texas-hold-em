import random
from Card import Card
import os

class Deck:
    #initialize deck with 52 cards
    def __init__(self):
        self.cards = []
        for i in range(2, 15):
            self.cards.append(Card("s", i))
            self.cards.append(Card('h', i))
            self.cards.append(Card('c', i))
            self.cards.append(Card('d', i))
        random.shuffle(self.cards)

    #reset deck to its original state
    def shuffle(self):
        self.__init__()

    #returns the top card and removes it from the deck
    def deal(self):
        return self.cards.pop(0)

def get_card_image_path(suit: str, value: int):
    all_cards = [card for card in os.listdir('assets')]

    if(value == 11):
        value = "jack"
    elif (value == 12):
        value = "queen"
    elif (value == 13):
        value = "king"
    elif (value == 14):
        value = "ace"

    value = str(value)

    for card in all_cards:
        if card.startswith(value) and suit in card:
            return os.path.join('assets', card)
