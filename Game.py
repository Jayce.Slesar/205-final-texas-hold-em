from get_hand_rankings import get_hand_rankings
from Player import *
from Card import *
from Deck import *


# test


#The game class is the main class for the entire program.
class Game:
    def __init__(self, big_blind_size, buy_in_size, num_computers):
        self.big_blind_size = big_blind_size
        self.buy_in_size = buy_in_size
        self.players = []
        self.players.append(UserPlayer("You",buy_in_size))
        for i in range(num_computers):
            self.players.append(ComputerPlayer("Computer " + str(i), buy_in_size))
        self.deck = Deck()
        self.pot = 0
        self.dealer = 0 #changes each round

        self.small_blind = 1 #players[small_blind] is the player that is the small blind
        self.big_blind = 2 #players[big_blind] is the player that is the big blind

        #variables for each round of betting
        self.current_bet = 0 #the current bet that must be called or raised upon to stay in the round
        self.bets = [0] * len(self.players) #each item in bets represents the amount each player has committed to the pot for this round of betting
        self.last_to_bet = 3 #if it is this player's turn to bet, either he is the first to bet, or the round of betting is complete
        self.community_cards = []

        self.one_player_left = False
        # this is false when all bets for a round have been collected, and true when bets still need to be collected
        self.betting_ongoing = False
        self.current_turn = 0  # whose turn it is to bet, this variable represents an index for self.players
        self.winners = [] #list of indicies corresponding to self.players that represent the player(s) who won the round

    #deals out two cards to each player, puts the big and small blind into the pot
    def deal_initial(self):
        for player in self.players:
            player.hand = [self.deck.deal(),self.deck.deal()]

        self.bets[self.small_blind] = self.big_blind_size / 2  # committs the small blind amount for the player who is the small blind
        self.bets[self.big_blind] = self.big_blind_size  # committs the big blind amount for the player who is the big blind
        self.players[self.small_blind].chips -= self.big_blind_size / 2
        self.players[self.big_blind].chips -= self.big_blind_size

        self.current_bet = self.big_blind_size

    #reveals the first 3 community cards
    def show_flop(self):
        for i in range(3):
            self.community_cards.append(self.deck.deal())

    #reveals the 4th community card
    def show_turn(self):
        self.community_cards.append(self.deck.deal())

    #reveals the last community cards
    def show_river(self):
        self.community_cards.append(self.deck.deal())

    #get things set up before the roudn of pre flop bets
    def start_pre_flop_betting(self):
        # betting starts and ends with the player to the left of the big blind
        if (self.big_blind == len(self.players) - 1):
            self.current_turn = 0
        else:
            self.current_turn = self.big_blind + 1

        self.last_to_bet = self.current_turn
        self.betting_ongoing = True

    # get the next bet in the round of betting. check to see if betting is over
    def get_next_bet(self, choice: int = None):

        # if the player whose turn it is is in
        if (self.players[self.current_turn].is_in):
            # collect their bet
            player_bet = self.players[self.current_turn].bet(self.bets[self.current_turn], self.current_bet,
                                                             self.get_max_chips(), choice)

            # if the player has raised
            if (player_bet + self.bets[self.current_turn] > self.current_bet):
                self.last_to_bet = self.current_turn
                # set the current bet to the amount the player already bet, plus the new amount they have bet
                self.current_bet = player_bet + self.bets[self.current_turn]

            # add the amount the player betted to the list of bets
            self.bets[self.current_turn] += player_bet

        # increment the counter for whose turn it is
        if (self.current_turn == (len(self.players) - 1)):
            self.current_turn = 0
        else:
            self.current_turn += 1

        if (self.last_to_bet == self.current_turn):
            self.betting_ongoing = False

            # round of betting is over, so add all bets to pot and reset bets to 0
            for i in range(len(self.bets)):
                self.pot += self.bets[i]
                self.bets[i] = 0

    # get things set up before the roudn of regular bets
    def start_betting(self):
        # check if there is only one player left
        num_players_in = 0
        for player in self.players:
            if (player.is_in):
                num_players_in += 1

        # if there is only one player left, skip round of betting
        if num_players_in == 1:
            self.betting_ongoing = False
            return

        self.current_bet = 0

        # betting starts and ends with the player to the left of the dealer
        if (self.dealer == len(self.players)):
            self.current_turn = 0
        else:
            self.current_turn = self.dealer + 1

        self.last_to_bet = self.current_turn
        self.betting_ongoing = True

    #helper function for get_bets(). Determines the least amount of chips that any player has. This is the most any player may bet.
    def get_max_chips(self):
        max_chips = self.players[0].chips + self.bets[0]
        for i, player in enumerate(self.players):
            if(player.is_in and player.chips + self.bets[i] < max_chips):
                max_chips = player.chips
        return max_chips

    #determines which player has the best hand.
    def determine_winner(self):
        hands_to_compare = [None] * len(self.players)

        #creates 7 card hands from each player consisting of their 2 cards and the 5 community cards.
        for i,player in enumerate(self.players):
            if player.is_in:
                hands_to_compare[i] = player.hand + self.community_cards

        #get a list of lists of integers. Each list of integers represents the value of a hand.
        hand_rankings = get_hand_rankings(hands_to_compare)

        #gets the winning hand, or hands if their is a tie for the best hand.
        self.winners = self.get_best_hands(hand_rankings)

    # distribute the pot to the winner(s)
    def distribute_pot(self):
        pot_portion = self.pot / len(self.winners)
        for winner in self.winners:#distribute pot
            pot_portion = round(pot_portion / 5) * 5
            self.players[winner].add_chips(pot_portion)
            self.pot -= pot_portion
            print("payed " + self.players[winner].name + " amount of: " + str(pot_portion))

        #if the there is not an even amount of money in the pot, distribute the rest of it.
        if(self.pot != 0):
            self.players[self.winners[0]].add_chips(self.pot)
            print("payed " + self.players[winner].name + " amount of: " + str(self.pot))

    #helper method for determine_winner(). Gets a list of integers that are the indicies of the winning players.
    def get_best_hands(self, hand_rankings):
        best_hands = []
        for i in range(len(hand_rankings)):
            best_hands.append(i)

        i = 0 #i is the index of each factor in a hand_ranking
        while (i < 6 and len(best_hands) != 1):#when all 6 hand_ranking factors have been compared or when a best hand is found
            max = 0
            for j, hand_ranking in enumerate(hand_rankings):
                    if(j in best_hands and hand_ranking[i] >= max):
                        max = hand_ranking[i]
            for j, hand_ranking in enumerate(hand_rankings):
                if(j in best_hands and hand_ranking[i] < max):
                    best_hands.remove(j)
            i += 1
        return best_hands

    #move on to the next round.
    def next_round(self):

        self.dealer += 1
        self.small_blind += 1
        self.big_blind += 1

        if(self.big_blind == len(self.players)):
            self.big_blind = 0

        elif(self.small_blind == len(self.players)):
            self.small_blind = 0

        elif(self.dealer == len(self.players)):
            self.dealer = 0

        self.deck.shuffle()

        #remove any players from the game that are out, unfold those players that are in.
        for i in reversed(range(len(self.players))):
            if(self.players[i].chips <= 0):
                self.players.pop(i)
            else:
                self.players[i].is_in = True

    #display a text representation of the game.
    def cmd_display(self):
        print("-" * 100)
        print("POT: " + str(self.pot) + "      COMMUNITY CARDS: " + str(self.community_cards))

        for i, player in enumerate(self.players):
            print(str(player.hand).ljust(20) + "  ", end='')
        print("")

        for i, player in enumerate(self.players):
            dealer = ""
            if (i == self.dealer):
                dealer = "dealer"
            print(dealer.ljust(20) + "  ", end='')
        print("")

        for i, player in enumerate(self.players):
            bet = "folded"
            if(player.is_in):
                bet = str(self.bets[i])
            print(bet.ljust(20) + "  ", end='')
        print("")

        for i, player in enumerate(self.players):
            print(player.name.ljust(20) + "  ", end='')
        print("")

        for i, player in enumerate(self.players):
            print(str(player.chips).ljust(20) + "  ", end='')
        print("")

        print("\n" + "-" * 100)


# def main():
#     print("Welcome to Texas Hold'em!")
#     g = Game(20,1000,3)
#
#     user_in_game = True
#     user_won = False
#
#     #Each loop is a round. Keep playing rounds until the user loses all chips or all other players lose.
#     while(user_in_game and (not user_won)):
#
#         found_user = False
#         for player in g.players:
#             if (type(player).__name__ == "UserPlayer"):
#                 found_user = True
#         if (not found_user):
#             print("You have lost all of your chips. You lose.")
#             user_in_game = False
#             break
#
#         if (len(g.players) == 2):
#             print("You are left with only one opponent. You win.")
#             user_won = True
#             break
#
#         elif (len(g.players) == 1):
#             print("You are the last player left. You win.")
#             user_won = True
#             break
#
#         print("new round!")
#         g.deal_initial()
#         g.cmd_display()
#         g.get_pre_flop_bets()
#
#         g.show_flop()
#         g.cmd_display()
#         g.get_bets()
#
#         g.show_turn()
#         g.cmd_display()
#         g.get_bets()
#
#         g.show_river()
#         g.cmd_display()
#         g.get_bets()
#
#         g.determine_winner()
#         g.cmd_display()
#
#         g.next_round()
#
# main()
