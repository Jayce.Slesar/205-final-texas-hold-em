import pygame
import settings
import play
from pygame.locals import *

def title():
    # Initialise screen
    pygame.init()
    screen = pygame.display.set_mode((1435, 765))

    pygame.display.set_caption("Texas Hold 'em ")

    # Fill background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 100, 0))

    # Display some text
    font = pygame.font.Font(None, 36)
    text = font.render("Welcome to Texas Hold 'em", 1, (10, 10, 10))
    textpos = text.get_rect()
    textpos.centerx = background.get_rect().centerx
    background.blit(text, textpos)


    # stores the width of the
    # screen into a variable
    width = screen.get_width()

    # stores the height of the
    # screen into a variable
    height = screen.get_height()

    button_color = (255,255,255)
    smallfont = pygame.font.SysFont(None, 35)
    play_text = smallfont.render('Play' , True , button_color)
    settings_text = smallfont.render('Settings' , True , button_color)
    quit_text = smallfont.render('Quit' , True , button_color)

    button_height = 40
    button_width = 140
    
    play_button_X = (width/2)-210-50
    play_button_Y = height/2
    
    settings_button_X = (width/2)+50+70
    settings_button_Y = height/2

    quit_button_X = width/2-70
    quit_button_Y = height-100
    # light shade of the button
    color_light = (170,170,170)
    
    # dark shade of the button
    color_dark = (100,100,100)

    # Blit everything to the screen
    screen.blit(background, (0, 0))

    # Event loop
    while 1:
        for ev in pygame.event.get():
          
            if ev.type == pygame.QUIT:
                pygame.quit()
                
            #checks if a mouse is clicked
            if ev.type == pygame.MOUSEBUTTONDOWN:
                
                #if the mouse is clicked on the
                # button the game is terminated
                if quit_button_X <= mouse[0] <= quit_button_X+button_width and quit_button_Y <= mouse[1] <= quit_button_Y+button_height:
                    pygame.quit()
                if play_button_X <= mouse[0] <= play_button_X+button_width and play_button_Y <= mouse[1] <= play_button_Y+button_height:
                    play.play()
                if settings_button_X <= mouse[0] <= settings_button_X+button_width and settings_button_Y <= mouse[1] <= settings_button_Y+button_height:
                    settings.settings()

        #get mous position
        mouse = pygame.mouse.get_pos()

        #disply quit box
        if quit_button_X <= mouse[0] <= quit_button_X+button_width and quit_button_Y <= mouse[1] <= quit_button_Y+button_height:
            pygame.draw.rect(screen,color_light,[quit_button_X,quit_button_Y,button_width,button_height])
        else:
            pygame.draw.rect(screen,color_dark,[quit_button_X,quit_button_Y,button_width,button_height])
        
        #display play box
        if play_button_X <= mouse[0] <= play_button_X+button_width and play_button_Y <= mouse[1] <= play_button_Y+button_height:
            pygame.draw.rect(screen,color_light,[play_button_X,play_button_Y,button_width,button_height])
        else:
            pygame.draw.rect(screen,color_dark,[play_button_X,play_button_Y,button_width,button_height])
        
        #disply settings box
        if settings_button_X <= mouse[0] <= settings_button_X+button_width and settings_button_Y <= mouse[1] <= settings_button_Y+button_height:
            pygame.draw.rect(screen,color_light,[settings_button_X,settings_button_Y,button_width,button_height])

        else:
            pygame.draw.rect(screen,color_dark,[settings_button_X,settings_button_Y,button_width,button_height])
      
        # superimposing the text onto our button
        screen.blit(play_text , (play_button_X,play_button_Y))
        screen.blit(settings_text , (settings_button_X, settings_button_Y))
        screen.blit(quit_text , (quit_button_X, quit_button_Y))

        # updates the frames of the game
        pygame.display.update()

        screen.blit(background, (0, 0))

