"""Player class for Texas Hold 'Em"""
from typing import Union
from chips import bin_chips_by_value, get_value_from_chips, ChipValue
from abc import ABC
import random
import time


class Player(ABC):
    def __init__(self, name: str, chips: int):
        """Initialize instance of the player class

        Args:
            name (str): name of player
            chips (int): Integer that will get optimally binned to ChipValue from the chips class in chips.py
        """
        self.name = name
        self.chips = chips
        self.hand = []
        self.is_in = True

    def add_chips(self, chips: Union[dict, int]):
        """Add chips to a player's balance from either datatype.

        Args:
            chips (Union[dict, int]): either a dictionary of ChipValue's or an integer amount.
        """

        if isinstance(chips, dict):
            new_chips_balance = get_value_from_chips(chips)
            self.chips = get_value_from_chips(self.chips + new_chips_balance)

        elif isinstance(chips, int):
            self.chips = self.chips + chips

#represents the player that is the user.
class UserPlayer(Player):
    def __init__(self, name: str, chips: int):
        super().__init__(name, chips)

    def bet(self, chips_already_bet: int, current_bet: int, max_bet: int, choice: int = None):
        print("\nIt is your turn.")
        print("Your cards are: " + str(self.hand))
        print("The current bet is " + str(current_bet))
        print("You have already bet " + str(chips_already_bet) + ". Choose an option.")

        if (current_bet == 0):
            if choice is None:
                choice = int(input("1: check\n2: bet"))
            else:
                choice = choice
            if (choice == 1):
                print("You check.")
                return 0
            elif (choice == 2):
                bet = int(input("enter bet amount: "))
                if (bet - chips_already_bet > max_bet):
                    print("bet is too large, bet has been changed to max chip amount")
                    bet = max_bet
                print("You bet " + str(bet) + " chips.")
                self.chips -= bet
                return bet
        else:
            choice = int(input("0: fold\n1: call\n2: raise"))
            if (choice == 0):
                print("You fold.")
                self.is_in = False
                return 0
            elif (choice == 1):
                print("You bet " + str(current_bet - chips_already_bet) + " more to call the bet of " + str(current_bet))
                self.chips -= current_bet - chips_already_bet
                return current_bet - chips_already_bet
            elif (choice == 2):
                bet = int(input("enter bet amount: "))
                if (bet - chips_already_bet > max_bet):
                    print("bet is too large, bet has been changed to max chip amount")
                    bet = max_bet - chips_already_bet
                if (bet < current_bet - chips_already_bet):
                    print("bet is too small, bet has been changed to min amount")
                    bet = current_bet - chips_already_bet
                print("You raise to " + str(bet + chips_already_bet) + ".")
                self.chips -= bet
                return bet


#represents a computer player.
class ComputerPlayer(Player):
    def __init__(self, name: str, chips: int):
        super().__init__(name, chips)

    def bet(self, chips_already_bet: int, current_bet: int, max_bet: int, choice: int = None):
        print("\nIt is " + self.name + "'s turn.")
        time.sleep(1)
        if (current_bet == 0):
            choice = random.choice([1,2])
            if (choice == 1):
                print(self.name + " checks.")
                return 0
            elif (choice == 2):
                bet = 100
                if (bet - chips_already_bet > max_bet):
                    bet = max_bet
                print(self.name + " bets " + str(bet) + " chips.")
                self.chips -= bet
                return bet
        else:
            choice = random.choice([0,1,2])
            if (choice == 0):
                print(self.name + " folds.")
                self.is_in = False
                return 0
            elif (choice == 1):
                print(self.name + " bets " + str(current_bet - chips_already_bet) + " more to call the bet of " + str(current_bet))
                self.chips -= current_bet - chips_already_bet
                return current_bet - chips_already_bet
            elif (choice == 2):
                bet = current_bet * 2
                if (bet - chips_already_bet > max_bet):
                    bet = max_bet - chips_already_bet
                if (bet < current_bet - chips_already_bet):
                    bet = current_bet - chips_already_bet
                print(self.name + " raises to " + str(bet + chips_already_bet) + ".")
                self.chips -= bet
                return bet
